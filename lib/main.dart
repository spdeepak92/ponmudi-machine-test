import 'package:flutter/material.dart';
import 'package:ponmudi/screens/home.dart';
import 'package:ponmudi/screens/webcam.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ponnmudi',
      theme: ThemeData(
        primaryColor: Colors.indigo,
        accentColor: Colors.indigoAccent,
      ),
      debugShowCheckedModeBanner: false,
      routes: {
        '/home': (context) => Home(),
        '/webcam': (context) => Webcam(),
      },
      initialRoute: '/home',
    );
  }
}
