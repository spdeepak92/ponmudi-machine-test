import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Webcam extends StatefulWidget {
  const Webcam({Key? key}) : super(key: key);

  @override
  _WebcamState createState() => _WebcamState();
}

class _WebcamState extends State<Webcam> {
  bool isLoading = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            WebView(
              initialUrl: 'https://webcamtoy.com/',
              javascriptMode: JavascriptMode.unrestricted,
              onPageFinished: (finish) {
                setState(() {
                  isLoading = false;
                });
              },
            ),
            isLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Stack(),
          ],
        ),
      ),
    );
  }
}
