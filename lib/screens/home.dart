import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final currentActiveSlider = PageController();
  List bannerImages = [
    'https://www.keralatourism.org/images/destination/large/ponmudi_hill_station_thiruvananthapuram20140412112853_243_1.jpg',
    'https://www.keralatourism.org/images/destination/large/ponmudi_hill_station_thiruvananthapuram20131128115532_243_1.jpg',
    'https://www.keralatourism.org/images/destination/large/ponmudi_hill_station_thiruvananthapuram20131031114431_243_1.jpg',
    'https://www.keralatourism.org/images/destination/large/ponmudi_hill_station_thiruvananthapuram20131108135356_243_1.jpg',
    'https://www.keralatourism.org/images/destination/large/ponmudi_hill_station_thiruvananthapuram20131108135356_243_3.jpg',
  ];

  Future webViewMethod() async {
    print('In Microphone permission method');
    await Permission.microphone.request();
    WebViewMethodForCamera();
  }

  Future WebViewMethodForCamera() async {
    await Permission.camera.request();
  }

  @override
  void initState() {
    webViewMethod();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.indigo,
      appBar: AppBar(
        title: Text(
          'Ponmudi',
          style: TextStyle(
            fontSize: width / 18,
            letterSpacing: 2.0,
          ),
        ),
        elevation: 0.0,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/webcam');
            },
            icon: Icon(
              Icons.camera_alt,
            ),
          ),
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.width * 0.55,
                child: PageView(
                  children: bannerImages.map((e) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: height / 20,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          image: DecorationImage(
                            image: NetworkImage(e),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    );
                  }).toList(),
                  controller: currentActiveSlider,
                ),
              ),
              SizedBox(
                height: 5.0,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'Near By Places',
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Kallar: A popular tourist destination near Ponmudi. The River Kallar here offers a good picnic spot in the form of Golden Valley. Here, there are options to trek in the adjoining forests. The Meenmutti waterfall is a major draw at Kallar.',
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Peppara Wildlife Sanctuary: This sanctuary spread over 53 sq. km. in the Western Ghats is accessible from Vithura. With its rich flora and fauna, Peppara dotted with hillocks and forests is a great attraction for wildlife enthusiasts.',
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Koyikkal Palace, Nedumangad: This ancient palace dates back to the 15th century. Museums of folklore and numismatics are its major attractions, besides the traditional architecture and layout of the palace.',
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'The Folklore Museum here is a treasure trove of quaint musical instruments, occupational implements, household utensils, models of folk art etc. Displayed in the Koyikkal Palace, they focus attention on the cultural background of the State.',
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
